FROM openjdk:8u131-jre-alpine

MAINTAINER Krisztián Kőszegi <krisztian.koszegi@gmail.com>

ENV USERNAME jrunner

RUN sed -i s_securerandom.source=file:/dev/random_securerandom.source=file:/dev/./urandom_ $JAVA_HOME/lib/security/java.security

RUN apk add --no-cache curl jq tcpdump libcap && \
    addgroup -g 1000 -S $USERNAME && \
    adduser -u 1000 -G $USERNAME -h /home/$USERNAME -s /bin/sh -S $USERNAME && \
    chgrp $USERNAME /usr/sbin/tcpdump && \
    setcap cap_net_raw+ep /usr/sbin/tcpdump

WORKDIR /home/$USERNAME

USER $USERNAME

CMD ["/bin/sh"]